﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Servicos.Interfaces
{
    public interface IClienteServicos
    {
        Task<IList<Cliente>> ObterTodos();
        Task<Cliente> Obter(int id);
        Task Salvar(Cliente cliente);
        Task Excluir(int id);
    }
}
