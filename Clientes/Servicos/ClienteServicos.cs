﻿using Dominio;
using Repositorio.Interfaces;
using Servicos.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Servicos
{
    public class ClienteServicos : IClienteServicos
    {
        private readonly IClienteRepositorio _clienteRepositorio;

        public ClienteServicos(IClienteRepositorio clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }
        public async Task Excluir(int id)
        {
            await _clienteRepositorio.Remover(id);
        }

        public async Task<Cliente> Obter(int id)
        {
            return await _clienteRepositorio.ObterPorId(id);
        }

        public async Task<IList<Cliente>> ObterTodos()
        {
            return await _clienteRepositorio.ObterTodos();
        }

        public async Task Salvar(Cliente cliente)
        {
            if (cliente == null)
                throw new ArgumentException("Cliente inválido");

            cliente.EValido();

            if (cliente.Id == 0)
            {
                await _clienteRepositorio.Adicionar(cliente);
            }
            else
            {
                await _clienteRepositorio.Atualizar(cliente);
            }
                
        }
    }
}
