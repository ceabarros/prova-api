﻿using Dominio;
using InfraEstrutura.Excecoes;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TesteUnitario
{
    /*if (string.IsNullOrEmpty(Logradouro))
                throw new NegocioExcecao("Logradouro é obrigatório");

            if (string.IsNullOrEmpty(Bairro))
                throw new NegocioExcecao("Bairro é obrigatório");

            if (string.IsNullOrEmpty(Cidade))
                throw new NegocioExcecao("Cidade é obrigatório");

            if (string.IsNullOrEmpty(Estado))
                throw new NegocioExcecao("Estado é obrigatório");

            if(Logradouro.Length > 50)
                throw new NegocioExcecao("Logradouro deve ter no máximo 50 caracteres");

            if (Bairro.Length > 40)
                throw new NegocioExcecao("Bairro deve ter no máximo 40 caracteres");

            if (Cidade.Length > 40)
                throw new NegocioExcecao("Cidade deve ter no máximo 40 caracteres");

            if (Estado.Length > 40)
                throw new NegocioExcecao("Estado deve ter no máximo 40 caracteres");*/
    public class EnderecoTeste
    {
        [Fact]
        public void DadoUmNovoEnderecoDeveSerValido()
        {
            var novoEndereco = new Endereco
            {
                Logradouro = "Rua do lado",
                Bairro = "Centro",
                Cidade = "Rio de Janeiro",
                Estado = "Rio de Janeiro"
            };

            novoEndereco.EValido();
        }

        [Fact]
        public void DadoUmNovoEnderecoSemLogradouroDeveGerarExcecao()
        {
            var novoEndereco = new Endereco
            {                
                Bairro = "Centro",
                Cidade = "Rio de Janeiro",
                Estado = "Rio de Janeiro"
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoEndereco.EValido());

            Assert.Equal("Logradouro é obrigatório", ex.Message);
        }

        [Fact]
        public void DadoUmNovoEnderecoSemBairroDeveGerarExcecao()
        {
            var novoEndereco = new Endereco
            {
                Logradouro = "Rua do lado",                
                Cidade = "Rio de Janeiro",
                Estado = "Rio de Janeiro"
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoEndereco.EValido());

            Assert.Equal("Bairro é obrigatório", ex.Message);
        }

        [Fact]
        public void DadoUmNovoEnderecoSemCidadeDeveGerarExcecao()
        {
            var novoEndereco = new Endereco
            {
                Logradouro = "Rua do lado",
                Bairro = "Centro",
                Estado = "Rio de Janeiro"
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoEndereco.EValido());

            Assert.Equal("Cidade é obrigatório", ex.Message);
        }

        [Fact]
        public void DadoUmNovoEnderecoSemEstadoDeveGerarExcecao()
        {
            var novoEndereco = new Endereco
            {
                Logradouro = "Rua do lado",
                Bairro = "Centro",
                Cidade = "Rio de Janeiro"
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoEndereco.EValido());

            Assert.Equal("Estado é obrigatório", ex.Message);
        }

        [Fact]
        public void DadoUmNovoEnderecoComLogradouroMaior50CaracteresDeveGerarExcecao()
        {
            var novoEndereco = new Endereco
            {
                Logradouro = "123456789012345678901234567890123456789012345678901234567890",
                Bairro = "Centro",
                Cidade = "Rio de Janeiro",
                Estado = "Rio de Janeiro"
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoEndereco.EValido());

            Assert.Equal("Logradouro deve ter no máximo 50 caracteres", ex.Message);
        }

        [Fact]
        public void DadoUmNovoEnderecoComBairroMaior40CaracteresDeveGerarExcecao()
        {
            var novoEndereco = new Endereco
            {
                Logradouro = "Rua do lado",
                Bairro = "12345678901234567890123456789012345678901234567890",
                Cidade = "Rio de Janeiro",
                Estado = "Rio de Janeiro"
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoEndereco.EValido());

            Assert.Equal("Bairro deve ter no máximo 40 caracteres", ex.Message);
        }

        [Fact]
        public void DadoUmNovoEnderecoComCidadeMaior40CaracteresDeveGerarExcecao()
        {
            var novoEndereco = new Endereco
            {
                Logradouro = "Rua do lado",
                Bairro = "Centro",
                Cidade = "12345678901234567890123456789012345678901234567890",
                Estado = "Rio de Janeiro"
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoEndereco.EValido());

            Assert.Equal("Cidade deve ter no máximo 40 caracteres", ex.Message);
        }

        [Fact]
        public void DadoUmNovoEnderecoComEstadoMaior40CaracteresDeveGerarExcecao()
        {
            var novoEndereco = new Endereco
            {
                Logradouro = "Rua do lado",
                Bairro = "Centro",
                Cidade = "Rio de Janeiro",
                Estado = "12345678901234567890123456789012345678901234567890"
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoEndereco.EValido());

            Assert.Equal("Estado deve ter no máximo 40 caracteres", ex.Message);
        }
    }
}
