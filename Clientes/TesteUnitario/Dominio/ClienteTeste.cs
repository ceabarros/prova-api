﻿using Dominio;
using InfraEstrutura.Excecoes;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TesteUnitario
{
    public class ClienteTeste
    {        
        [Fact]
        public void DadoUmNovoClientePrecisaSerValido()
        {
            var novoCliente = new Cliente
            {
                Nome = "Carlos Eduardo",
                CPF = "12345678901",
                DataNascimento = DateTime.Now,
                Endereco = new Endereco
                {
                    Logradouro = "Rua do lado",
                    Cidade = "Rio de Janeiro",
                    Estado = "Rio de Janeiro",
                    Bairro = "Praça Seca"
                }
            };

            novoCliente.EValido();
        }

        [Fact]
        public void DadoUmNovoClienteSemNomeDeveGerarExcecao()
        {
            var novoCliente = new Cliente
            {
                CPF = "12345678901",
                DataNascimento = DateTime.Now,
                Endereco = new Endereco
                {
                    Logradouro = "Rua do lado",
                    Cidade = "Rio de Janeiro",
                    Estado = "Rio de Janeiro",
                    Bairro = "Praça Seca"
                }
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoCliente.EValido());

            Assert.Equal("Nome obrigatório", ex.Message);

        }
        [Fact]
        public void DadoUmNovoClienteSemCPFDeveGerarExcecao()
        {
            var novoCliente = new Cliente
            {
                Nome = "Carlos Eduardo",
                DataNascimento = DateTime.Now,
                Endereco = new Endereco
                {
                    Logradouro = "Rua do lado",
                    Cidade = "Rio de Janeiro",
                    Estado = "Rio de Janeiro",
                    Bairro = "Praça Seca"
                }
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoCliente.EValido());

            Assert.Equal("CPF obrigatório", ex.Message);
        }
        [Fact]
        public void DadoUmNovoClienteSemDataNascimentoDeveGerarExcecao()
        {
            var novoCliente = new Cliente
            {
                Nome = "Carlos Eduardo",
                CPF = "12345678901",
                Endereco = new Endereco
                {
                    Logradouro = "Rua do lado",
                    Cidade = "Rio de Janeiro",
                    Estado = "Rio de Janeiro",
                    Bairro = "Praça Seca"
                }
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoCliente.EValido());

            Assert.Equal("Data de nascimento obrigatório", ex.Message);
        }
        [Fact]
        public void DadoUmNovoClienteSemEnderecoDeveGerarExcecao()
        {
            var novoCliente = new Cliente
            {
                Nome = "Carlos Eduardo",
                CPF = "12345678901",
                DataNascimento = DateTime.Now
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoCliente.EValido());

            Assert.Equal("Endereço obrigatório", ex.Message);
        }
        [Fact]
        public void DadoUmNovoClienteComNomeMaior30CaracteresDeveGerarExcecao() {
            var novoCliente = new Cliente
            {
                Nome = "1234567890123456789012345678901234567890",
                CPF = "12345678901",
                DataNascimento = DateTime.Now,
                Endereco = new Endereco
                {
                    Logradouro = "Rua do lado",
                    Cidade = "Rio de Janeiro",
                    Estado = "Rio de Janeiro",
                    Bairro = "Praça Seca"
                }
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoCliente.EValido());

            Assert.Equal("Nome deve ter até 30 caracteres", ex.Message);
        }
        [Fact]
        public void DadoUmNovoClienteComCPFInvalidoDeveGerarExcecao() {
            var novoCliente = new Cliente
            {
                Nome = "Carlos Eduardo",
                CPF = "123",
                DataNascimento = DateTime.Now,
                Endereco = new Endereco
                {
                    Logradouro = "Rua do lado",
                    Cidade = "Rio de Janeiro",
                    Estado = "Rio de Janeiro",
                    Bairro = "Praça Seca"
                }
            };

            var ex = Assert.Throws<NegocioExcecao>(() => novoCliente.EValido());

            Assert.Equal("CPF inválido", ex.Message);
        }
    }
}
