﻿using System;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Repositorio.Interfaces;
using Servicos;
using Dominio;

namespace TesteUnitario.Servicos
{
    public class ClienteServicosTeste
    {
        [Fact]
        public async Task DadoUmClienteValidoDeveSalvar()
        {
            var cliente = new Cliente
            {
                Nome = "Carlos Eduardo",
                CPF = "12345678901",
                DataNascimento = DateTime.Now,
                Endereco = new Endereco
                {
                    Logradouro = "rua do centro",
                    Bairro = "centro",
                    Cidade = "Rio de Janeiro",
                    Estado = "Rio de Janeiro"
                },
            };

            var repositorioMock = new Mock<IClienteRepositorio>();
            repositorioMock.Setup(r => r.Adicionar(cliente));
            var servico = new ClienteServicos(repositorioMock.Object);
            await servico.Salvar(cliente);

            repositorioMock.VerifyAll();
        }

        [Fact]
        public async Task DadoUmClienteInValidoDeveGerarExcecao()
        {   
            
            var servico = new ClienteServicos(null);            

            var ex = await Assert.ThrowsAsync<ArgumentException>(() => servico.Salvar(null));

            Assert.Equal("Cliente inválido", ex.Message);
        }
    }
}
