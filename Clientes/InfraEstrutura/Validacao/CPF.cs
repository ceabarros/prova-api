﻿namespace InfraEstrutura.Validacao
{
    public static class CPF
    {
        public static bool EValido(string valor)
        {
            long numero = 0;
            if (!long.TryParse(valor, out numero))
                return false;

            if (valor.Length != 11)
                return false;

            return true;
        }
    }
}
