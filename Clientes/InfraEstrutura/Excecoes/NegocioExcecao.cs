﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraEstrutura.Excecoes
{
    public class NegocioExcecao : Exception
    {
        public NegocioExcecao(string message) : base(message)
        {
        }

        public NegocioExcecao(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
