﻿using Dominio;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositorio.Map
{
    public class EnderecoMap : IEntityTypeConfiguration<Endereco>
    {
        public void Configure(EntityTypeBuilder<Endereco> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Logradouro)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(p => p.Bairro)
                .IsRequired()
                .HasColumnType("varchar(40)");
            
            builder.Property(p => p.Cidade)
                .IsRequired()
                .HasColumnType("varchar(40)");

            builder.Property(p => p.Estado)
                .IsRequired()
                .HasColumnType("varchar(40)");

            builder.ToTable("Enderecos");
        }
    }
}
