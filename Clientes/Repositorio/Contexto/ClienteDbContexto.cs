﻿
using Dominio;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Repositorio.Contexto
{
    public class ClienteDbContexto : DbContext
    {
        public ClienteDbContexto(DbContextOptions options) : base(options)
        { }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;

            base.OnModelCreating(modelBuilder);
        }
    }
}
