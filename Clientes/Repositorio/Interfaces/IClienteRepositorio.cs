﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio.Interfaces
{
    public interface IClienteRepositorio
    {
        Task Adicionar(Cliente cliente);
        Task<Cliente> ObterPorId(int id);
        Task<List<Cliente>> ObterTodos();
        Task Atualizar(Cliente cliente);
        Task Remover(int id);
    }
}
