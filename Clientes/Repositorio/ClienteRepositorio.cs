﻿using Dominio;
using Microsoft.EntityFrameworkCore;
using Repositorio.Contexto;
using Repositorio.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio
{
    public class ClienteRepositorio : RepositorioBase<Cliente>, IClienteRepositorio
    {
        public ClienteRepositorio(ClienteDbContexto db) : base(db)
        {
        }

        public override async Task<Cliente> ObterPorId(int id)
        {
            return await Db.Clientes
                .Include(c => c.Endereco)
                .FirstOrDefaultAsync(c => c.Id == id);
        }


        public override async Task<List<Cliente>> ObterTodos()
        {
            return await Db.Clientes
                .Include(c => c.Endereco)
                .ToListAsync();
        }

        public override async Task Remover(int id)
        {
            var endereco = await Db.Enderecos.FirstOrDefaultAsync(e => e.ClienteId == id);

            if (endereco != null)
                Db.Enderecos.Remove(endereco);

            Db.Clientes.Remove(new Cliente { Id = id });

            await Db.SaveChangesAsync();
        }

    }
}
