﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.ViewModel;
using AutoMapper;
using Dominio;
using InfraEstrutura.Excecoes;
using Microsoft.AspNetCore.Mvc;
using Servicos.Interfaces;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IClienteServicos _clienteServicos;

        public ClientesController(IMapper mapper, IClienteServicos clienteServicos)
        {
            _mapper = mapper;
            _clienteServicos = clienteServicos;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClienteViewModel>>> Get()
        {
            var clientes = _mapper.Map<IEnumerable<ClienteViewModel>>(await _clienteServicos.ObterTodos());
            return Ok(clientes);
        }

        // GET api/values/5
        [HttpGet("{id:int}")]
        public async Task<ActionResult<ClienteViewModel>> Get(int id)
        {
            var cliente = _mapper.Map<ClienteViewModel>(await _clienteServicos.Obter(id));
            if (cliente == null)
                return NotFound("Cliente não encontrado");

            return Ok(cliente);
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ClienteViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var cliente = _mapper.Map<Cliente>(model);

            try
            {
                await _clienteServicos.Salvar(cliente);
                return Ok();

            }
            catch (NegocioExcecao ex)
            {
                return BadRequest(ex.Message);
            }
        }

    
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] ClienteViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var cliente = _mapper.Map<Cliente>(model);

            if (cliente.Id == 0)
                return BadRequest("Identificador inválido");

            try
            {
                await _clienteServicos.Salvar(cliente);
                return Ok();

            }
            catch (NegocioExcecao ex)
            {
                return BadRequest(ex.Message);
            }
        }

        
        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _clienteServicos.Excluir(id);
            return Ok();
        }
    }
}