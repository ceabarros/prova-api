﻿using Microsoft.Extensions.DependencyInjection;
using Repositorio;
using Repositorio.Contexto;
using Repositorio.Interfaces;
using Servicos;
using Servicos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Configuracao
{
    public static class InjecaoDependenciaConfig
    {
        public static IServiceCollection ResolveDependencias(this IServiceCollection services)
        {
            services.AddScoped<ClienteDbContexto>();
            services.AddScoped<IClienteServicos, ClienteServicos>();
            services.AddScoped<IClienteRepositorio, ClienteRepositorio>();
            return services;
        }
    }
}
