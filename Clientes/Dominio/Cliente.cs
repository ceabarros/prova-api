﻿using InfraEstrutura.Excecoes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio
{
    public class Cliente : Entidade
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public DateTime DataNascimento { get; set; }

        public Endereco Endereco { get; set; }

        public override void EValido()
        {
            if (string.IsNullOrEmpty(Nome))
                throw new NegocioExcecao("Nome obrigatório");

            if (Nome.Length > 30)
                throw new NegocioExcecao("Nome deve ter até 30 caracteres");

            if (string.IsNullOrEmpty(CPF))
                throw new NegocioExcecao("CPF obrigatório");

            if (!InfraEstrutura.Validacao.CPF.EValido(CPF))
                throw new NegocioExcecao("CPF inválido");

            if (DataNascimento == DateTime.MinValue)
                throw new NegocioExcecao("Data de nascimento obrigatório");

            if (Endereco == null)
                throw new NegocioExcecao("Endereço obrigatório");

            Endereco.EValido();
        }
    }
}
