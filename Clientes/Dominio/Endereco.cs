﻿using InfraEstrutura.Excecoes;

namespace Dominio
{
    public class Endereco : Entidade
    {
        
        public string Logradouro { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }

        public int ClienteId { get; set; }
        public Cliente Cliente { get; set; }

        public override void EValido()
        {
            if (string.IsNullOrEmpty(Logradouro))
                throw new NegocioExcecao("Logradouro é obrigatório");

            if (string.IsNullOrEmpty(Bairro))
                throw new NegocioExcecao("Bairro é obrigatório");

            if (string.IsNullOrEmpty(Cidade))
                throw new NegocioExcecao("Cidade é obrigatório");

            if (string.IsNullOrEmpty(Estado))
                throw new NegocioExcecao("Estado é obrigatório");

            if(Logradouro.Length > 50)
                throw new NegocioExcecao("Logradouro deve ter no máximo 50 caracteres");

            if (Bairro.Length > 40)
                throw new NegocioExcecao("Bairro deve ter no máximo 40 caracteres");

            if (Cidade.Length > 40)
                throw new NegocioExcecao("Cidade deve ter no máximo 40 caracteres");

            if (Estado.Length > 40)
                throw new NegocioExcecao("Estado deve ter no máximo 40 caracteres");
        }
    }
}
