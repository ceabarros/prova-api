﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio
{
    public abstract class Entidade
    {
        public int Id { get; set; }
        public abstract void EValido();
    }
}
